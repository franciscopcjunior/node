const usuarios =
[
    {
        "nome": "Francisco",
        "idade": 36,
        "cidade": "Pindamonhangaba",
        "email": "francisco@emeio.com",
        "documentos": {
            "rg": "256989852",
            "cpf": "33366699988"
        },
        "carros": [
            "Ferrari",
            "BMW",
            "Lotus"
        ]
    },
    {
        "nome": "Carlos",
        "idade": 38,
        "cidade": "Barueri",
        "email": "carlos@emeio.com",
        "documentos": {
            "rg": "256989849",
            "cpf": "33366699756"
        },
        "carros": [
            "Gol",
            "Uno"
        ]
    },
    {
        "nome": "Paula",
        "idade": 37,
        "cidade": "Barueri",
        "email": "paula@emeio.com",
        "documentos": {
            "rg": "256989850",
            "cpf": "33366699689"
        },
        "carros": [
            "Siena"
        ]
    }
]

const usuario = usuarios[1].nome

console.log(usuario)