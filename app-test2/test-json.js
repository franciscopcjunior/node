const usuarios = 
[
    {
        "nome": "William",
        "idade": 20,
        "cidade": "Pindamonhangaba",
        "email": "williambiolgia05@gmail.com",
        "documentos": {
            "rg": "504583897",
            "cpf": "47906435974"
        },
        "carros": [
            "Opala"
        ]
    },
    {
        "nome": "Luan",
        "idade": 23,
        "cidade": "Pinda",
        "email": "luan@emai.com",
        "documentos": {
            "rg": "456463456",
            "cpf": "343546446"
        },
        "carros": [
            "brasilia",
            "crosfox",
            "parati"
        ]
    },
    {
        "nome": "Paulo",
        "idade": 25,
        "cidade": "Roseira",
        "email": "paulo@yahoo.com.br",
        "documentos": {
            "rg": "670202853",
            "cpf": "2935884178"
        },
        "carros": [
            "monza",
            "savero",
            "opala"
        ]
    }
]  

usuarios.forEach(function(usuario) {

    console.log(`Nome: ${usuario.nome}`);
    console.log(`Idade: ${usuario.idade}`);
    console.log(`RG: ${usuario.documentos.rg}`);

    usuario.carros.forEach(function (carro) {
        console.log(carro);
    });
    console.log('-------');
});


// const nome = usuarios[1].nome;
// const cidade = usuarios[1].cidade; 
// const rg = usuarios[1].documentos.rg;
// const cpf = usuarios[1].documentos.cpf;
// const carros = usuarios[1].carros[1];
// console.log(nome);
// console.log(cidade);
// console.log(rg);
// console.log(cpf);
// console.log(carros);