const axios = require('axios');

axios.get("http://senacao.tk/objetos/veiculo_array_objeto")
.then(function(res) {
    //console.log(res.data);
    const car = res.data;
    console.log("----------Veículo----------")
    console.log(`Marca: ${car.marca}`);
    console.log(`Modelo: ${car.modelo}`);
    console.log(`Ano: ${car.ano}`);
    console.log(`KM: ${car.quilometragem}`);

    car.opcionais.forEach(function(opcionais, index) {
        console.log(`Opcional ${index+1}: ${opcionais}`);
});
    console.log("----------Vendedor----------");
    console.log(`Nome: ${car.vendedor.nome}`);
    console.log(`Idade: ${car.vendedor.idade}`);
    console.log(`Celular: ${car.vendedor.celular}`);
    console.log(`Cidade: ${car.vendedor.cidade}`);
    console.log("");
    console.log("***************************************************");  
});


axios.get("http://senacao.tk/objetos/computador_array_objeto")
.then(function(res) {
    //console.log(res.data);
    const computer = res.data;
    console.log("----------Computador----------")
    console.log(`Marca: ${computer.marca}`);
    console.log(`Modelo: ${computer.modelo}`);
    console.log(`Memoria: ${computer.memoria}`);
    console.log(`SSD: ${computer.ssd}`);
    console.log(`Processador: ${computer.processador}`);

    computer.softwares.forEach(function(softwares, index) {
        console.log(`Software ${index+1}: ${softwares}`);
    });
        console.log("----------Fornecedor----------");
        console.log(`Razão Social: ${computer.fornecedor.rsocial}`);
        console.log(`Telefone: ${computer.fornecedor.telefone}`);
        console.log(`Endereço: ${computer.fornecedor.endereco}`);
        
        console.log("");
        console.log("***************************************************");
});
    